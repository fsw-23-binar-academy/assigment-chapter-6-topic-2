"use strict";
const { DataTypes } = require("sequelize");
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("comments", "updated_at", {
      type: DataTypes.DATE,
    });
    await queryInterface.addColumn("comments", "deleted_at", {
      type: DataTypes.DATE,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn("comments", "updated_at", {
      type: DataTypes.DATE,
    });
    await queryInterface.removeColumn("comments", "deleted_at", {
      type: DataTypes.DATE,
    });
  },
};
