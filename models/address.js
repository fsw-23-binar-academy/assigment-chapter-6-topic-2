"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Users }) {
      // define association here
    }
  }
  Address.init(
    {
      country: DataTypes.STRING,
      city: DataTypes.STRING,
      postal_code: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: "created_at",
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: "updated_at",
      },
      deleted_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Address",
      tableName: "address",
    }
  );
  return Address;
};
