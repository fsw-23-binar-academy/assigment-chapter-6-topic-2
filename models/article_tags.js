"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class article_tags extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Article, tags: Tags }) {
      Article.belongsToMany(Tags, {
        through: "article_tags",
        foreignKey: "article_id",
        otherKey: "tag_id",
      });
    }
  }
  article_tags.init(
    {
      article_id: DataTypes.INTEGER,
      tag_id: DataTypes.INTEGER,
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,

        defaultValue: sequelize.fn("now"),
      },
    },
    {
      sequelize,
      modelName: "article_tags",
    }
  );
  return article_tags;
};
