"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Users, Article, Comment }) {
      // define association here
      Comment.belongsTo(Users, {
        foreignKey: "user_id",
        as: "creator",
      });
      //   Comment.belongsTo(Article, {
      //     as: "comments",
      //     foreignKey: "article_id",
      //   });
    }
  }
  Comment.init(
    {
      comment: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: "created_at",
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        field: "updated_at",
      },
      deleted_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Comment",
      tableName: "comments",
    }
  );
  return Comment;
};
