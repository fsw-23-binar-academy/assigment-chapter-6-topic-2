"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tags extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Article, tags: Tags }) {
      // define association here
      Tags.belongsToMany(Article, {
        through: "article_tags",
        foreignKey: "tag_id",
        otherKey: "article_id",
        as: "articles",
      });
    }
  }
  tags.init(
    {
      name: DataTypes.STRING,
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "tags",
    }
  );
  return tags;
};
