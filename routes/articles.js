var express = require("express");
var router = express.Router();

const { Article, Comment, Users, tags: Tags } = require("../models");

router.get("/", (req, res) => {
  Article.findAll({
    order: ["id"],

    include: [
      {
        model: Users,
        as: "author",
      },
      {
        model: Tags,
        as: "tags",
        attributes: ["name"],
      },
    ],
  })
    .then((articles) => {
      console.log(articles);
      res.json({
        data: articles,
      });
    })
    .catch((err) => {
      console.log(err);
      res.json({
        error: "err",
      });
    });
});

router.get("/with-comments", (req, res) => {
  Article.findAll({
    order: ["id"],

    include: [
      {
        model: Comment,
        as: "comments",
        include: [
          {
            model: Users,
            as: "creator",
            attributes: ["username", "email"],
          },
        ],
      },
    ],
  })
    .then((articles) => {
      res.json({
        data: articles,
      });
    })
    .catch((err) => {
      console.log(err);
      res.json({
        err,
      });
    });
});

router.get("/pagination", (req, res) => {
  const query = req.query;
  const { limit = 5, page } = query;
  const offset = (page - 1) * limit;

  Article.findAndCountAll({
    order: ["id"],
    limit,
    offset,
  })
    .then((articles) => {
      res.json({
        articles,
        pages: Math.ceil(articles.count / limit),
      });
    })
    .catch((err) => {
      res.json({
        err,
      });
    });
});

router.get("/:id", (req, res) => {
  Article.findOne({
    where: {
      id: req.params.id,
    },
  })
    .then((data) => {
      res.json({
        data,
      });
    })
    .catch((err) =>
      res.json({
        err,
      })
    );
});

router.post("/", (req, res) => {
  const { title, content, user_id = 1 } = req.body;
  Article.create({
    title,
    content,
    user_id,
  })
    .then((data) => {
      res.json({
        message: "Succesfully create new article",
        data,
      });
    })
    .catch((err) => {
      console.log("err", err);
      res.json({
        message: err.message,
      });
    });
});

module.exports = router;
