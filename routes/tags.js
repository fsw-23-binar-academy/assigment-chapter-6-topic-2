var express = require("express");
var router = express.Router();

const { tags: Tags, Article, Comment, Users } = require("../models");

/* GET users listing. */
router.get("/", (req, res) => {
  Tags.findAll({
    include: [
      {
        model: Article,
        as: "articles",
        attributes: ["id", "title", "createdAt", "updatedAt"],
        // include: [
        //   {
        //     model: Users,
        //     as: "author",
        //     attributes: ["username", "email"],
        //   },
        //   {
        //     model: Comment,
        //     as: "comments",
        //     include: [
        //       {
        //         model: Users,
        //         as: "creator",
        //         attributes: ["username", "email"],
        //       },
        //     ],
        //   },
        // ],
      },
    ],
  })
    .then((data) => {
      res.json({
        message: "Success",
        data,
      });
    })
    .catch((err) => {
      console.log(err);
      res.json({ message: "error", err });
    });
});

module.exports = router;
