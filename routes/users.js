var express = require("express");
var router = express.Router();

const { Users, Article, Address } = require("../models");

/* GET users listing. */
router.get("/", (req, res) => {
  Users.findAll({
    include: [
      {
        model: Article,
        as: "articles",
        attributes: ["id", "title", "createdAt", "updatedAt"],
      },
      {
        model: Address,
        as: "address",
      },
    ],
  })
    .then((data) => {
      res.json({
        message: "Success",
        data,
      });
    })
    .catch((err) => {
      console.log(err);
      res.json({ message: "error", err });
    });
});

router.post("/", (req, res) => {
  Users.create({
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
  })
    .then(() => res.json({ message: "Succesfully created new user" }))
    .catch((err) => {
      res.status(400).json({
        message: err.message,
      });
    });
});

router.get("/:id", (req, res) => {
  const { id } = req.params;
  Users.findOne({
    where: {
      id,
    },
  })
    .then((data) => {
      res.json({
        data,
        message: "success",
      });
    })
    .catch((err) => {
      res.json({
        err,
      });
    });
});

module.exports = router;
