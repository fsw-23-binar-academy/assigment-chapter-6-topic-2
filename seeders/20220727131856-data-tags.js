"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    let data = [];
    const tags = [
      "Sport",
      "Food",
      "Technology",
      "Travel",
      "Music",
      "Art",
      "Movie",
      "Business",
      "Education",
    ];
    tags.forEach((tag) => {
      data.push({
        name: tag,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    });

    // await queryInterface.bulkInsert("tags", data);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    // await queryInterface.bulkDelete("tags", null, {});
  },
};
