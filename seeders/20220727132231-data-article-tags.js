"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    let data = [];
    for (let i = 1; i <= 8; i++) {
      let article_id = i;
      let tag_id = i;
      if (i <= 2) {
        article_id = 1;
      }

      data.push({
        article_id,
        tag_id,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }
    // await queryInterface.bulkInsert("article_tags", data);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
